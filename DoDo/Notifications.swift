//
//  Notifications.swift
//  DoDo
//
//  Created by Nikolay S on 16.03.2021.
//  Copyright © 2021 Sozinov Nikolay. All rights reserved.
//

import Foundation
import UserNotifications

enum NotificationType {
    case add, edit, delete
}

class Notifications: NSObject, UNUserNotificationCenterDelegate {
    
    let notificationCenter = UNUserNotificationCenter.current()
    let todo = Todo()
    
    func userRequest() {
        
        let options: UNAuthorizationOptions = [.alert, .sound, .badge]
        
        notificationCenter.requestAuthorization(options: options) {
            (didAllow, error) in
            if !didAllow {
                print("User has declined notifications")
            }
        }
    }
    
    func scheduleNotification(notificationType:NotificationType, todoItem:TodoItem) {
 
        let content = UNMutableNotificationContent()
        let userActions = todoItem.id// ?? "userActions"
        
        content.title = "ToDo"
        content.body = todoItem.title // ?? "This is example how to create"
        content.sound = UNNotificationSound.default
        content.badge = 1
        content.categoryIdentifier = userActions
        
        let newComponents = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute,.second,], from: todoItem.date)
        let trigger = UNCalendarNotificationTrigger(dateMatching: newComponents, repeats: false)
        
        let identifier = userActions //"Local Notification"
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        switch notificationType {
        case .add:
            guard todoItem.date >= Date() else {return}
            notificationCenter.add(request) { (error) in
                if let error = error {
                    print("Error \(error.localizedDescription)")
                }
            }
        case .edit:
            notificationCenter.removeDeliveredNotifications(withIdentifiers: [userActions])
            scheduleNotification(notificationType: .add, todoItem: todoItem)
            print("edit")
        case .delete:
            notificationCenter.removeDeliveredNotifications(withIdentifiers: [userActions])
            print("delete")
        }
        
        let snoozeAction = UNNotificationAction(identifier: "Snooze", title: "Snooze one min", options: [])
        let deleteAction = UNNotificationAction(identifier: "Delete", title: "Delete", options: [.destructive])
        let category = UNNotificationCategory(identifier: userActions,
                                              actions: [snoozeAction, deleteAction],
                                              intentIdentifiers: [],
                                              options: [])
        
        notificationCenter.setNotificationCategories([category])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([.alert,.sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        switch response.actionIdentifier {
        case UNNotificationDismissActionIdentifier:
            print("Dismiss Action")
        case UNNotificationDefaultActionIdentifier:
            print("Default")
        case "Snooze":
            print("Snooze")
            let testTODO = TodoItem(id: response.notification.request.content.categoryIdentifier,
                                    title: "I need search todo by ID", //TODO: find the text of the note array by id and insert here
                                    isDone: false,
                                    colorName: "blue",
                                    date: Date() + 60)
            scheduleNotification(notificationType: .edit, todoItem: testTODO)
        case "Delete":
            print("Delete")
        default:
            print("Unknown action")
        }
        completionHandler()
    }
}
