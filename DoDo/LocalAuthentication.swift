//
//  LocalAuthentication.swift
//  DoDo
//
//  Created by Nikolay S on 15.03.2021.
//  Copyright © 2021 Sozinov Nikolay. All rights reserved.
//

import LocalAuthentication
import SwiftUI

class LocalAuthentication: ObservableObject {
    
    @Published var isUnlocked: Bool = false
    
    func authenticate() {
        let context = LAContext()
        var error: NSError?

        // check whether biometric authentication is possible
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            // it's possible, so go ahead and use it
            let reason = "We need to unlock your data."

            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) { success, authenticationError in
                // authentication has now completed
                DispatchQueue.main.async {
                    if success {
                        // authenticated successfully
                        print("successfully")
                        self.isUnlocked = true
                    } else {
                        // there was a problem
                        print("problem")
                        self.isUnlocked = false
                    }
                }
            }
        } else {
            // no biometrics
            print("no biometrics")
            self.isUnlocked = false
        }
    }
}

