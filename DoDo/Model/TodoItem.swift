//
//  TodoItem.swift
//  DoDo
//
//  Created by Nikolay S on 14.03.2021.
//  Copyright © 2021 Sozinov Nikolay. All rights reserved.
//

import Foundation
import SwiftUI

// Model
// Our data stores as such structures.
struct TodoItem: Identifiable, Codable {
    var id:String = UUID().uuidString
    var title: String
    var isDone: Bool = false
    var colorName: String = "teal"
    var date: Date
}
