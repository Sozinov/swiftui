//
//  ParentDatePicker.swift
//  DoDo
//
//  Created by Nikolay S on 16.03.2021.
//  Copyright © 2021 Sozinov Nikolay. All rights reserved.
//

import SwiftUI

struct ParentDatePicker: View  {
    @State var showGraphical: Bool = false
    @State var currentDate: Date = Date()
    @Binding var choosenDate: Date

    var body: some View {
        Button(action: {
            showGraphical.toggle()
        }) {
            Text("\(currentDate.dateToString)")
                .fontWeight(.medium)
        }
        .padding()
        .background(Color(.systemGray5))
        .cornerRadius(8)
        //        .sheet(isPresented: $showGraphical, content: {
        //            CusDatePicker(currentDate: $currentDate)
        //        })
        .sheet(isPresented: $showGraphical) {
            self.choosenDate = currentDate
        } content: {
            CusDatePicker(currentDate: $currentDate)
        }
        
    }
}
struct CusDatePicker: View {
    @Binding var currentDate: Date
    
    var body: some View {
        //https://developer.apple.com/documentation/swiftui/datepicker
        if #available(iOS 14.0, *) {
            DatePicker("Date", selection: $currentDate, displayedComponents: [.date, .hourAndMinute])
                //.datePickerStyle(CompactDatePickerStyle())
                .datePickerStyle(GraphicalDatePickerStyle())
        } else {
            // Fallback on earlier versions
        }
    }
}

struct CusDatePicker_Previews: PreviewProvider {
    static var previews: some View {
        ParentDatePicker(choosenDate: .constant(Date()))
    }
}
