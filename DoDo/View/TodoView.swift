//
//  TodoView.swift
//  DoDo
//
//  Created by Nikolay S on 14.03.2021.
//  Copyright © 2021 Sozinov Nikolay. All rights reserved.
//

import SwiftUI

struct TodoView: View {
    
    // Connection to the ViewModel (Todo)
    @EnvironmentObject var todo: Todo
    
    @ObservedObject var localAuthentication = LocalAuthentication()
    
    // UI content and layout
    // ---------------------
    
    var body: some View {
        NavigationView {
            if localAuthentication.isUnlocked {
                ZStack {
                    ListView()
                    .navigationBarTitle("Todo")
                    NewItemButton()
                }
            } else {
                VStack {
                    Spacer()
                    Text("Locked")
                        .font(.largeTitle)
                        .foregroundColor(.red)
                    Spacer()
                    Button(action: {
                        localAuthentication.authenticate()
                    }) {
                        Text("Authenticate")
                            .fontWeight(.medium)
                    }
                    Spacer()
                    Button(action: {
                        localAuthentication.isUnlocked = true
                    }) {
                        Text("Enter NoFaceID")
                            .fontWeight(.light)
                            .font(.system(size: 12))
                    }
                    Spacer()
                }
            }
        }
        .onAppear {
            localAuthentication.authenticate()
            UITableView.appearance().backgroundColor = UIColor.clear
        }
    }
}

struct TodoView_Previews: PreviewProvider {
    static var previews: some View {
        TodoView().environmentObject(Todo())
    }
}

