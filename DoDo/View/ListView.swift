//
//  ListView.swift
//  DoDo
//
//  Created by Nikolay S on 14.03.2021.
//  Copyright © 2021 Sozinov Nikolay. All rights reserved.
//

import SwiftUI

struct ListView: View {
    
    // Color scheme
    @Environment(\.colorScheme) var colorScheme
    
    // Connection to the ViewModel (Todo)
    @EnvironmentObject var todo: Todo
    
    // UI content and layout
    // ---------------------
    
    var body: some View {
        if #available(iOS 14.0, *) {
            ScrollView {
                LazyVStack {
                    ForEach(self.todo.items.sorted(by: { $0.date < $1.date})) { item in
                        RowView(itemId: item.id)
                    }
                }
                .padding(20)
            }

        } else {
            // Fallback on earlier versions
            List{
                ForEach(self.todo.items.sorted(by: { $0.date < $1.date})) { item in
                    RowView(itemId: item.id)
                }
                .listRowBackground(colorScheme == .light ? Color.white : Color.black)
            }
            .animation(.default)
            .onAppear() {
                UITableView.appearance().separatorStyle = .none
            }
        }
    }
}

struct ListView_Previews: PreviewProvider {
    static var previews: some View {
        ListView().environmentObject(Todo())
    }
}
