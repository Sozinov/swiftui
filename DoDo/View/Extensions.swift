//
//  Extensions.swift
//  DoDo
//
//  Created by Nikolay S on 16.03.2021.
//  Copyright © 2021 Sozinov Nikolay. All rights reserved.
//

import UIKit

extension Date {
    var dateToString: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy HH:mm"
        return dateFormatter.string(from: self)
    }
    
    var dateToDouble: Double {
        return Double(self.timeIntervalSince1970)
    }
}
