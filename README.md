# DoDo #
#### A complete Task Tracking application made with Swift using Xcode 11 & SwiftUI framework for iOS 13. ###

### Main features: ###
1. **MVVM**-based app architecture.
2. CRUD functionality.
3. Unique **UI** and **UX** design.
4. Custom animations.
5. **Secure** data storage.
6. Synchronization via iTunes.
7. **Light and Dark** appearances.
8. **LocalAuthentication** FaceID.
9.  Local Notification witn Action


### Your task is to create a TODO-app. ###
### Requirements for the app: ###
#### Must have: ####
* As a user I want to manage tasks (add/update/remove).
* As a user I want to change task state (completed/not completed).
* As a user I want to prohibit unwanted people from accessing my tasks.
* As a user I want the tasks to be persisted
 
#### Should have: ####
* As a user I want to unlock the app with biometrics
 
#### Could have: ####
* As a user I want to set due date for tasks.
* As a user I want to be notified of overdue tasks that haven’t been completed in time.
 
#### Things to keep in mind: ####
* Good architectural principles.
* Design guidelines.
 
Basically, we try to keep this assignment quite simple and the responsibility how to develop this and take this forward, we give to the person himself… :)


## UPD 30.05.2121 ##
add test app: **Gesture** in SwiftUI

### More? ###
Personal project, includes unit tests.
* [Mac App Store](https://apps.apple.com/ru/app/jsonhelper/id1536132004)
* [JSON Helper](https://bitbucket.org/Sozinov/json-helper/src/master/)
