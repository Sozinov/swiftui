//
//  ContentView.swift
//  GestureInSwiftUI
//
//

import SwiftUI

struct ContentView: View {
    @State private var offsetCardView = CGSize.zero
    @State private var offcetBuyView = CGSize.zero
    @State private var isShowingDetails = false
    @State private var appearDetails = false
    @State private var isShowBuyView = false
    
    @Namespace private var namespace
    
    private let width = UIScreen.main.bounds.width
    private let height = UIScreen.main.bounds.height
    
    private func getRotationAmount() -> Double {
        let max = UIScreen.main.bounds.width / 2
        let currentAmount = offsetCardView.width
        let percentage = Double(currentAmount / max)
        return percentage * 10
    }
}

extension ContentView {
    var body: some View {
        ZStack {
            LinearGradient(
                gradient: Gradient(colors: [.red, .blue]),
                startPoint: .top,
                endPoint: .bottom)
            VStack(spacing: 40) {
                //TODO: Think about how not to show two controllers without an artifact
               // if !isShowingDetails {
                    
                    CardView(namespace: namespace)
                        .offset(offsetCardView)
                        .rotationEffect(Angle(degrees: getRotationAmount()))
                        .onTapGesture {
                            isShowingDetails.toggle()
                        }
                        .gesture(
                            DragGesture()
                                .onChanged({ value in
                                    offsetCardView = value.translation
                                })
                                .onEnded({ _ in
                                    //Down swipe card rigt or left
                                    if offsetCardView.width > width / 2 {
                                        offsetCardView = CGSize(width: width, height: height)
                                    }
                                    else if offsetCardView.width < ((width / 2) * -1) {
                                        offsetCardView = CGSize(width: ((width / 2) * -1), height: height)
                                    }
                                    else {
                                        offsetCardView = .zero
                                    }
                                })
                        )
                    ArrowButton(offset: $offsetCardView)
               // }
            }
            if isShowingDetails {
                DetailsView(isShowing: $isShowingDetails,
                            offset: $offcetBuyView,
                            buyViewIsPresenteed: $isShowBuyView,
                            namespace: namespace,
                            appear: appearDetails)
                    .onAppear {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                            appearDetails.toggle()
                        }
                    }
                    .onDisappear {
                        appearDetails.toggle()
                    }
            }
            BuyView()
                .offset(offcetBuyView)
                .offset(y: isShowBuyView ? 0 : height)
                .gesture(
                    DragGesture()
                        .onChanged({ value in
                            offcetBuyView = value.translation
                        })
                        .onEnded({ _ in
                            if offcetBuyView.height > height / 5 {
                                isShowBuyView.toggle()
                            } else {
                                offcetBuyView = .zero
                            }
                        })
                )
                .padding(.top, 40)
        }
        .ignoresSafeArea()
        .animation(.spring())
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
