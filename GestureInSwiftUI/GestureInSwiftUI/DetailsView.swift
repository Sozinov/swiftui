//
//  DetailsView.swift
//  GestureInSwiftUI
//
//

import SwiftUI

struct DetailsView: View {
    
    @Binding var isShowing:Bool
    @Binding var offset:CGSize
    @Binding var buyViewIsPresenteed:Bool
    var namespace:Namespace.ID
    var appear:Bool
    
    var body: some View {
        VStack(spacing: 12) {
            HStack {
                if appear {
                    DismissButton(isShowing: $isShowing)
                }
                Spacer()
            }.frame(height: 70)
            
            Image("SwiftUI")
                .resizable()
                .matchedGeometryEffect(id: "ID_Image", in: namespace)
                .frame(width: 200, height: 200)
                .padding(.top)
            Text("SwiftUI")
                .font(.title)
                .fontWeight(.bold)
                .foregroundColor(.orange)
                .matchedGeometryEffect(id: "ID_txt", in: namespace)
                .padding(.horizontal, 20)
            if appear {
                Text("Project_irst_Create_cstm_views".localized)
                    .transition(.scale)
                    .font(.system(size: 20))
                    .foregroundColor(.white)
                
                HStack(spacing: 15) {
                    Button(action: {showBuyView()}) {
                        Text("Buy_block".localized)
                            .fontWeight(.bold)
                            .foregroundColor(.white)
                            .padding(.vertical)
                            .frame(width: UIScreen.main.bounds.width - 150)
                            .background(Color.orange)
                            .clipShape(Capsule())
                    }
                    
                    Button(action: {}) {
                        Image(systemName: "suit.heart.fill")
                            .font(.title)
                            .foregroundColor(.red)
                            .padding()
                            .background(Color.white)
                            .clipShape(Circle())
                            .shadow(radius: 3)
                    }
                }
                .transition(.scale)
                .padding(.top, 25)
            }
            Spacer()
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(
            Color.black
                .matchedGeometryEffect(id: "ID_background", in: namespace)
        )
    }
    private func showBuyView() {
        offset = .zero
        buyViewIsPresenteed.toggle()
    }
    
}

//struct DetailsView_Previews: PreviewProvider {
//    static var previews: some View {
//        DetailsView()
//    }
//}
