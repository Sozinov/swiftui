//
//  CardView.swift
//  GestureInSwiftUI
//
//

import SwiftUI

struct CardView: View {
    var namespace:Namespace.ID
    
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 25)
                .matchedGeometryEffect(id: "ID_background", in: namespace)
                .frame(width: 300, height: 400)
                .shadow(color: .black, radius: 20, x: 10, y: 10)
                .foregroundColor(.black)
            HStack(spacing: 10) {
                Image("SwiftUI")
                    .resizable()
                    .matchedGeometryEffect(id: "ID_Image", in: namespace)
                    .frame(width: 100, height: 100)
                Text("SwiftUI")
                    .font(.title)
                    .fontWeight(.bold)
                    .foregroundColor(.orange)
                    .matchedGeometryEffect(id: "ID_txt", in: namespace)
            }
        }
        .animation(.spring())
    }
}

//struct CardView_Previews: PreviewProvider {
//    static var previews: some View {
//        CardView()
//    }
//}
