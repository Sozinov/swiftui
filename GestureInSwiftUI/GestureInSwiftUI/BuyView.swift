//
//  BuyView.swift
//  GestureInSwiftUI
//
//

import SwiftUI

struct BuyView: View {
    var body: some View {
        VStack(spacing: 20) {
            Text("Congratulations".localized)
                .font(.largeTitle)
            Text("The_course_was_successfully_paid".localized)
                .font(.title)
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(Color.white)
        .cornerRadius(25)
        .animation(.spring())
        .foregroundColor(.orange)
        .multilineTextAlignment(.center)
    }
}

struct BuyView_Previews: PreviewProvider {
    static var previews: some View {
        BuyView()
    }
}
