//
//  String+Extension.swift
//  GestureInSwiftUI
//
//  Created by Nikolay S on 30.04.2021.
//

import Foundation
extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
