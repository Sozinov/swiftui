//
//  GestureInSwiftUIApp.swift
//  GestureInSwiftUI
//
//

import SwiftUI

@main
struct GestureInSwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
